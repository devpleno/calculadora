import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      display: '',
      result: ''
    }
  }

  handleOP(op) {

    if (op === 'C') {
      
      this.setState({
        display: '',
        result: ''
      })

    } else if (op === '=') {
      
      this.setState({
        display: this.state.result,
        result: ''
      })

    } else {
      
      const display = this.state.display + op
      let result = this.state.result

      try {
        let fixOperation = display.split('x').join('*')
        fixOperation = fixOperation.split('÷').join('/')
        fixOperation = fixOperation.split(',').join('.')

        result = new String(eval(fixOperation)).toString()
      } catch(e){}

      this.setState({
        display: display,
        result: result
      })

    }

  }

  render() {

    const col1Buttons = [
      ['7', '8', '9'],
      ['4', '5', '6'],
      ['1', '2', '3'],
      [',', '0', '=']
    ]

    const col2Buttons = ['C', '÷', 'x', '-', '+']

    return (
      <View style={styles.container}>
        <Text style={styles.display}>{this.state.display}</Text>
        <Text style={styles.result}>{this.state.result}</Text>

        <View style={styles.buttons}>
          <View style={styles.col1}>

            {col1Buttons.map((line, k) => {
              return (
                <View style={styles.line} key={k}>

                  {line.map((btn, i) => {
                    return (
                      <TouchableOpacity key={i} style={styles.btn} onPress={() => this.handleOP(btn)}>
                        <Text style={styles.btnText}>{btn}</Text>
                      </TouchableOpacity>
                    )
                  })}

                </View>
              )
            })}

          </View>
          <View style={styles.col2}>
            {col2Buttons.map((btn, i) => {
              return (
                <TouchableOpacity key={i} style={styles.btn} onPress={() => this.handleOP(btn)}>
                  <Text style={styles.btnText}>{btn}</Text>
                </TouchableOpacity>
              )
            })}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  display: {
    flex: 1,
    backgroundColor: '#EFEFEF',
    fontSize: 80,
    textAlign: 'right',
    paddingTop: 30,
    paddingRight: 10
  },
  result: {
    flex: 0.4,
    backgroundColor: '#EFEFEF',
    fontSize: 40,
    textAlign: 'right',
    paddingBottom: 10,
    paddingRight: 10
  },
  line: {
    flex: 1,
    flexDirection: 'row'
  },
  btn: {
    flex: 1,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#222222',
    justifyContent: 'center'
  },
  btnText: {
    fontSize: 50,
    textAlign: 'center',
    color: 'white'
  },
  buttons: {
    flex: 5,
    flexDirection: 'row'
  },
  col1: {
    flex: 3,
    backgroundColor: '#000'
  },
  col2: {
    flex: 1,
    backgroundColor: '#0B0B0B'
  }
});
